(function ($, Drupal, undefined) {
	$(document).ready(function(){
		$(function () {
		  'use strict';

		  var console = window.console || { log: function () {} };
		  var $images = $('.jquery-viewer');

		  var options = {
		    // inline: true,
		    url: 'data-original',
		    ready: function (e) {
		      console.log(e.type);
		    },
		    show: function (e) {
		      console.log(e.type);
		    },
		    shown: function (e) {
		      console.log(e.type);
		    },
		    hide: function (e) {
		      console.log(e.type);
		    },
		    hidden: function (e) {
		      console.log(e.type);
		    },
		    view: function (e) {
		      console.log(e.type);
		    },
		    viewed: function (e) {
		      console.log(e.type);
		    }
		  };

		  $images.on({
		    ready:  function (e) {
		      console.log(e.type);
		    },
		    show:  function (e) {
		      console.log(e.type);
		    },
		    shown:  function (e) {
		      console.log(e.type);
		    },
		    hide:  function (e) {
		      console.log(e.type);
		    },
		    hidden: function (e) {
		      console.log(e.type);
		    },
		    view:  function (e) {
		      console.log(e.type);
		    },
		    viewed: function (e) {
		      console.log(e.type);
		    }
		  }).viewer(options);
		});



	});
})(jQuery, Drupal);
