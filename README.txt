
-- SUMMARY --

This module provides a field formatter in order to display image fields using Fengyuan Chen's jquery-viewer library.

For a full description of the module, visit the project page:
  http://drupal.org/project/jquery_viewer
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/jquery_viewer


-- REQUIREMENTS --

* @todo


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* @todo


-- USAGE --

* @todo


-- CONTACT --

Current maintainers:
* Mustafa Akbay (makbay) - http://drupal.org/user/3518933

